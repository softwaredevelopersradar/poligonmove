﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.SeriesXY;
using Arction.Wpf.SemibindableCharting.Views.ViewXY;

namespace PolygonMove
{
    public class PolygonMoveClass
    {
        private void InitializeDeploymentKey()
        {
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            //Set Deployment Key for semi - bindable chart, if you use it
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }



        private double _GlobalRangeXmin;
        public double GlobalRangeXmin
        {
            get { return _GlobalRangeXmin; }
            set
            {
                if (_GlobalRangeXmin != value)
                {
                    _GlobalRangeXmin = value;
                    ReCalc();
                }
            }
        }

        private void ReCalc()
        {
            _GlobalRangeXmax = _GlobalRangeXmin + _GlobalNumberOfBands * _GlobalBandWidthMHz;
            _PolygonSeries.Clear();
            InitPolygons();
            InitLineCollection();
        }

        private double _GlobalRangeXmax = 0;
        private double GlobalRangeXmax
        {
            get { return _GlobalRangeXmax; }
            set { if (_GlobalRangeXmax != value) _GlobalRangeXmax = value; }
        }

        private double _GlobalRangeYmin = -120;
        public double GlobalRangeYmin
        {
            get { return _GlobalRangeYmin; }
            set
            {
                if (_GlobalRangeYmin != value)
                {
                    _GlobalRangeYmin = value;
                    //SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
                }
            }
        }

        private double _GlobalRangeYmax = 0;
        public double GlobalRangeYmax
        {
            get { return _GlobalRangeYmax; }
            set
            {
                if (_GlobalRangeYmax != value)
                {
                    _GlobalRangeYmax = value;
                    //SetYRange(_GlobalRangeYmin, _GlobalRangeYmax);
                }
            }
        }

        private int _GlobalNumberOfBands;
        public int GlobalNumberOfBands
        {
            get { return _GlobalNumberOfBands; }
            set
            {
                if (_GlobalNumberOfBands != value)
                {
                    _GlobalNumberOfBands = value;
                    ReCalc();
                }
            }
        }

        private double _GlobalBandWidthMHz;
        public double GlobalBandWidthMHz
        {
            get { return _GlobalBandWidthMHz; }
            set
            {
                if (_GlobalBandWidthMHz != value)
                {
                    _GlobalBandWidthMHz = value;
                    ReCalc();
                }
            }
        }

        private bool _MoveX = false;
        public bool MoveX
        {
            get { return _MoveX; }
            set
            {
                if (_MoveX != value)
                {
                    _MoveX = value;
                }
            }
        }

        private bool _MoveY = true;
        public bool MoveY
        {
            get { return _MoveY; }
            set
            {
                if (_MoveY != value)
                {
                    _MoveY = value;
                }
            }
        }

        private bool _isEditable = false;
        public bool isEditable
        {
            get { return _isEditable; }
            set
            {
                if (_isEditable != value)
                {
                    _isEditable = value;
                }
            }
        }

        private bool _isEditablePro = false;
        private bool isEditablePro
        {
            get { return _isEditablePro; }
            set
            {
                if (_isEditablePro != value)
                {
                    _isEditablePro = value;
                }
            }
        }



        LightningChartUltimate _LightningChartUltimate;

        PolygonSeriesCollection _PolygonSeries = new PolygonSeriesCollection();

        Color colorFirst = Color.FromArgb(255, 255, 192, 0);
        Color colorLast = Color.FromArgb(255, 255, 94, 0);

        LineCollectionCollection _LineCollections = new LineCollectionCollection();

        public PolygonMoveClass(LightningChartUltimate LightningChartUltimate, double GlobalRangeXmin, int GlobalNumberOfBands, double GlobalBandWidthMHz)
        {
            InitializeDeploymentKey();

            _LightningChartUltimate = LightningChartUltimate;

            _GlobalRangeXmin = GlobalRangeXmin;
            _GlobalNumberOfBands = GlobalNumberOfBands;
            _GlobalBandWidthMHz = GlobalBandWidthMHz;

            _LightningChartUltimate.ViewXY.PolygonSeries = _PolygonSeries;
            _LightningChartUltimate.ViewXY.LineCollections = _LineCollections;

            _LightningChartUltimate.MouseMove += Chart_MouseMove;
            _LightningChartUltimate.MouseUp += _LightningChartUltimate_MouseUp;

            ReCalc();
        }

        public PolygonMoveClass(LightningChartUltimate LightningChartUltimate, IEnumerable<CustomBand> CustomBands)
        {
            InitializeDeploymentKey();

            _LightningChartUltimate = LightningChartUltimate;

            _LightningChartUltimate.ViewXY.PolygonSeries = _PolygonSeries;
            _LightningChartUltimate.ViewXY.LineCollections = _LineCollections;

            _LightningChartUltimate.MouseMove += Chart_MouseMove;
            _LightningChartUltimate.MouseUp += _LightningChartUltimate_MouseUp;

            Calc(CustomBands);
        }

        private void Calc(IEnumerable<CustomBand> CustomBands)
        {
            _PolygonSeries.Clear();
            InitPolygons(CustomBands);
            InitLineCollection();
        }

        public IEnumerable<CustomBand> GetCustomBands()
        {
            List<CustomBand> GetCustomBandsList = new List<CustomBand>();

            for (int i = 0; i < _PolygonSeries.Count(); i++)
            {
                _PolygonSeries[i].GetMinMaxValues(out double xMin, out double XMax, out double yMin, out double yMax);

                CustomBand customBand = new CustomBand
                {
                    StartXMHz = xMin,
                    WidthMHz = XMax - xMin,
                    Ypos = yMax - yMin
                };

                GetCustomBandsList.Add(customBand);
            }

            return GetCustomBandsList;
        }

        private void InitPolygons()
        {
            for (int i = 0; i < GlobalNumberOfBands; i++)
            {
                _PolygonSeries.Add(AddPolygon(GeneratePointDouble2D(GlobalBandWidthMHz * i + GlobalRangeXmin + GlobalBandWidthMHz / 2, GlobalBandWidthMHz / 2), ChartTools.CalcGradient(colorFirst, colorLast, 0)));
            }
        }

        private void InitPolygons(IEnumerable<CustomBand> CustomBands)
        {
            for (int i = 0; i < CustomBands.Count(); i++)
            {
                _PolygonSeries.Add(AddPolygon(GeneratePointDouble2D(CustomBands.ElementAt(i).StartXMHz + CustomBands.ElementAt(i).WidthMHz / 2, CustomBands.ElementAt(i).WidthMHz / 2, CustomBands.ElementAt(i).Ypos), ChartTools.CalcGradient(colorFirst, colorLast, 0)));
            }
        }

        public void InitLineCollection()
        {
            _LineCollections.Clear();

            //Line collection 2: arbitrary polyline
            LineCollection lineCollection2 = new LineCollection();
            lineCollection2.LineStyle.Color = Colors.Yellow;
            lineCollection2.LineStyle.Width = 1;
            //lineCollection2.LineStyle.Pattern = LinePattern.Dash;
            lineCollection2.LineStyle.Pattern = LinePattern.Solid;
            lineCollection2.MouseInteraction = false;
            lineCollection2.MouseHighlight = MouseOverHighlight.None;

            lineCollection2.Lines = LinkSegmentLines(GenerateSegmentLines(_PolygonSeries));

            //lineCollection2.Title.Text = "Polyline";

            _LineCollections.Add(lineCollection2);
        }

        private List<SegmentLine> GenerateSegmentLines(PolygonSeriesCollection PolygonSeries)
        {
            List<SegmentLine> segmentLines = new List<SegmentLine>();

            for (int i = 0; i < PolygonSeries.Count; i++)
            {
                PolygonSeries[i].GetMinMaxValues(out double xMin, out double XMax, out double yMin, out double yMax);

                SegmentLine segmentLine = new SegmentLine()
                {
                    AX = xMin,
                    AY = (yMin + yMax) / 2,
                    BX = XMax,
                    BY = (yMin + yMax) / 2
                };

                segmentLines.Add(segmentLine);
            }

            return segmentLines;
        }

        private SegmentLine[] LinkSegmentLines(List<SegmentLine> segmentLines)
        {
            for (int i = 0; i < segmentLines.Count - 1; i++)
            {
                SegmentLine segmentLine = new SegmentLine()
                {
                    AX = segmentLines[i].BX,
                    AY = segmentLines[i].BY,
                    BX = segmentLines[i+1].AX,
                    BY = segmentLines[i+1].AY
                };

                segmentLines.Insert(i+1,segmentLine);

                i++;
            }

            return segmentLines.ToArray();
        }

        private PointDouble2D[] GeneratePointDouble2D(double startXCentalMHz, double bandWidthMHz, double centralY = -80, double widthY = 1)
        {
            return new PointDouble2D[] {
                new PointDouble2D(startXCentalMHz - bandWidthMHz, centralY - widthY),
                new PointDouble2D(startXCentalMHz - bandWidthMHz, centralY + widthY),
                new PointDouble2D(startXCentalMHz + bandWidthMHz, centralY + widthY),
                new PointDouble2D(startXCentalMHz + bandWidthMHz, centralY - widthY)
            };
        }

        private PolygonSeries AddPolygon(PointDouble2D[] edgePoints, Color color, string title = "", bool intersectionsAllowed = false)
        {
            PolygonSeries series = new PolygonSeries();
            series.MouseHighlight = MouseOverHighlight.None;

            //series.MouseInteraction = false;

            series.Fill.GradientFill = GradientFill.Solid;
            series.Fill.Color = color;
            series.Title.Visible = true;
            series.Title.Color = Color.FromArgb(255, 30, 30, 30);
            series.Title.Text = title;
            series.Title.Shadow.Style = TextShadowStyle.DropShadow;
            series.Points = edgePoints;
            series.IntersectionsAllowed = intersectionsAllowed;

            series.MouseDown += Series_MouseDown;
            series.MouseUp += Series_MouseUp;

            series.MouseDoubleClick += Series_MouseDoubleClick;

            return series;
        }

        private bool sdown = false;
        private Point startPos;

        private double startxValue;
        private double startyValue;

        private int IndexOf = 0;


        private void Series_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var ps = (PolygonSeries)sender;

                //Console.WriteLine(ps.Title.Text);

                IndexOf = _PolygonSeries.IndexOf(ps);

                //Console.WriteLine(_PolygonSeries.IndexOf(ps));

                sdown = true;

                startPos = e.GetPosition(_LightningChartUltimate);

                _LightningChartUltimate.ViewXY.XAxes[0].CoordToValue((int)startPos.X, out var xValue, true);
                _LightningChartUltimate.ViewXY.YAxes[0].CoordToValue((int)startPos.Y, out var yValue, true);

                startxValue = xValue;
                startyValue = yValue;

                _EPOintValue = (int)yValue;

                Mouse.OverrideCursor = MouseCursors.SizeNS;
            }

            if (isEditable)
                if (e.RightButton == MouseButtonState.Pressed)
                {
                    var ps = (PolygonSeries)sender;

                    IndexOf = _PolygonSeries.IndexOf(ps);

                    var temppos = e.GetPosition(_LightningChartUltimate);

                    _LightningChartUltimate.ViewXY.XAxes[0].CoordToValue((int)temppos.X, out var xValue, true);
                    _LightningChartUltimate.ViewXY.YAxes[0].CoordToValue((int)temppos.Y, out var yValue, true);


                    PointDouble2D[] points4 = new PointDouble2D[_PolygonSeries[IndexOf].Points.Count()];
                    _PolygonSeries[IndexOf].Points.CopyTo(points4, 0);

                    PointDouble2D[] points1 = new PointDouble2D[] {
                    points4[0],
                    points4[1],
                    new PointDouble2D(xValue, points4[1].Y),
                    new PointDouble2D(xValue, points4[0].Y) };

                    PointDouble2D[] points2 = new PointDouble2D[] {
                    new PointDouble2D(xValue, points4[3].Y),
                    new PointDouble2D(xValue, points4[2].Y),
                    points4[2],
                    points4[3] };

                    _PolygonSeries.RemoveAt(IndexOf);

                    _PolygonSeries.Insert(IndexOf, AddPolygon(points2, ChartTools.CalcGradient(colorFirst, colorLast, 0)));
                    _PolygonSeries.Insert(IndexOf, AddPolygon(points1, ChartTools.CalcGradient(colorFirst, colorLast, 0)));

                    InitLineCollection();
                }

            if (isEditable)
                if (e.MiddleButton == MouseButtonState.Pressed)
                {
                    if (_PolygonSeries.Count > 1)
                    {
                        var ps = (PolygonSeries)sender;

                        IndexOf = _PolygonSeries.IndexOf(ps);

                        var isunited = false;

                        if (IndexOf + 1 < _PolygonSeries.Count)
                        //объединить справа
                        {
                            PointDouble2D[] points1 = new PointDouble2D[4];
                            _PolygonSeries[IndexOf].Points.CopyTo(points1, 0);

                            PointDouble2D[] points2 = new PointDouble2D[4];
                            _PolygonSeries[IndexOf + 1].Points.CopyTo(points2, 0);


                            PointDouble2D[] points4 = new PointDouble2D[] {
                    points1[0],
                    points1[1],
                    new PointDouble2D(points2[2].X, points1[1].Y),
                    new PointDouble2D(points2[3].X, points1[0].Y) };

                            _PolygonSeries.RemoveAt(IndexOf);
                            _PolygonSeries.RemoveAt(IndexOf);

                            _PolygonSeries.Insert(IndexOf, AddPolygon(points4, ChartTools.CalcGradient(colorFirst, colorLast, 0)));

                            isunited = true;
                        }
                        if (IndexOf == _PolygonSeries.Count - 1 && _PolygonSeries.Count > 1 && isunited == false)
                        //объединить слева
                        {
                            PointDouble2D[] points1 = new PointDouble2D[4];
                            _PolygonSeries[IndexOf].Points.CopyTo(points1, 0);

                            PointDouble2D[] points2 = new PointDouble2D[4];
                            _PolygonSeries[IndexOf - 1].Points.CopyTo(points2, 0);

                            PointDouble2D[] points4 = new PointDouble2D[] {
                        new PointDouble2D(points2[0].X, points1[0].Y),
                        new PointDouble2D(points2[1].X, points1[1].Y),
                        points1[2],
                        points1[3]
                        };

                            _PolygonSeries.RemoveAt(IndexOf - 1);
                            _PolygonSeries.RemoveAt(IndexOf - 1);

                            _PolygonSeries.Insert(IndexOf - 1, AddPolygon(points4, ChartTools.CalcGradient(colorFirst, colorLast, 0)));
                        }

                        InitLineCollection();
                    }
                }
        }

        private void Series_MouseUp(object sender, MouseEventArgs e)
        {
            sdown = false;
            Mouse.OverrideCursor = MouseCursors.Default;
        }

        private void Series_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (isEditablePro)
                if (e.RightButton == MouseButtonState.Pressed)
                {
                    var ps = (PolygonSeries)sender;

                    IndexOf = _PolygonSeries.IndexOf(ps);

                    var temppos = e.GetPosition(_LightningChartUltimate);

                    _LightningChartUltimate.ViewXY.XAxes[0].CoordToValue((int)temppos.X, out var xValue, true);
                    _LightningChartUltimate.ViewXY.YAxes[0].CoordToValue((int)temppos.Y, out var yValue, true);


                    PointDouble2D[] points4 = new PointDouble2D[_PolygonSeries[IndexOf].Points.Count()];
                    _PolygonSeries[IndexOf].Points.CopyTo(points4, 0);

                    PointDouble2D[] points1 = new PointDouble2D[] {
                    points4[0],
                    points4[1],
                    new PointDouble2D(xValue, points4[1].Y),
                    new PointDouble2D(xValue, points4[0].Y) };

                    PointDouble2D[] points2 = new PointDouble2D[] {
                    new PointDouble2D(xValue, points4[3].Y),
                    new PointDouble2D(xValue, points4[2].Y),
                    points4[2],
                    points4[3] };

                    _PolygonSeries.RemoveAt(IndexOf);

                    _PolygonSeries.Insert(IndexOf, AddPolygon(points2, ChartTools.CalcGradient(colorFirst, colorLast, 0)));
                    _PolygonSeries.Insert(IndexOf, AddPolygon(points1, ChartTools.CalcGradient(colorFirst, colorLast, 0)));

                    //VXY.PolygonSeries = polygonSeries;
                }
        }

        private PointDouble2D GetCentralPoint(PointDouble2D[] pointDouble2Ds)
        {
            double x = (pointDouble2Ds[2].X - pointDouble2Ds[1].X) / 2d + pointDouble2Ds[1].X;
            double y = (pointDouble2Ds[1].Y - pointDouble2Ds[0].Y) / 2d + pointDouble2Ds[0].Y;
            return new PointDouble2D(x, y);
        }


        public delegate void EPOintValueProEventHandler(int EPO, int value, double startMHz, double endMHz);
        public event EPOintValueProEventHandler EPOValuePro;

        private int _EPOintValue = -80;
        private int EPOintValue
        {
            get { return _EPOintValue; }
            set
            {
                if (_EPOintValue != value)
                {
                    _EPOintValue = value;

                    _PolygonSeries[IndexOf].GetMinMaxValues(out double xMin, out double XMax, out double yMin, out double yMax);
                    EPOValuePro?.Invoke(IndexOf, value, xMin, XMax);
                }
            }
        }

        private void SetLines(int bandIndex, double value)
        {
            int index = bandIndex * 2;

            _LineCollections[0].Lines[index] = new SegmentLine()
            {
                AX = _LineCollections[0].Lines[index].AX,
                AY = value,
                BX = _LineCollections[0].Lines[index].BX,
                BY = value
            };

            if (index != 0)
            {
                _LineCollections[0].Lines[index - 1] = new SegmentLine()
                {
                    AX = _LineCollections[0].Lines[index - 1].AX,
                    AY = _LineCollections[0].Lines[index - 1].AY,
                    BX = _LineCollections[0].Lines[index].AX,
                    BY = _LineCollections[0].Lines[index].AY
                };
            }
             
            if (index != _LineCollections[0].Lines.Count() - 1)
            {
                _LineCollections[0].Lines[index + 1] = new SegmentLine()
                {
                    AX = _LineCollections[0].Lines[index].BX,
                    AY = _LineCollections[0].Lines[index].BY,
                    BX = _LineCollections[0].Lines[index + 1].BX,
                    BY = _LineCollections[0].Lines[index + 1].BY
                };
            }

            _LineCollections[0].InvalidateData();
        }

        public void SetEPOValue(int EPO, int value)
        {
            if (EPO >= 0 && EPO < _GlobalNumberOfBands && value >= _GlobalRangeYmin && value <= _GlobalRangeYmax)
            {
                var currentPolygon = _PolygonSeries.ElementAt(EPO);
                currentPolygon.Points = GeneratePointDouble2D(
                    GlobalBandWidthMHz * EPO + GlobalRangeXmin + GlobalBandWidthMHz / 2,
                    GlobalBandWidthMHz / 2,
                    value);
            }
        }

        public void SetEPOsValues(IEnumerable<int> EPOs, IEnumerable<int> values)
        {
            int minCount = Math.Min(EPOs.Count(), values.Count());

            for (int i = 0; i < minCount; i++)
            {
                SetEPOValue(EPOs.ElementAt(i), values.ElementAt(i));
            }
        }

        private void Chart_MouseMove(object sender, MouseEventArgs e)
        {
            //if (e.LeftButton == MouseButtonState.Released)
            {
                var pos = e.GetPosition(_LightningChartUltimate);

                if (sdown)
                {
                    _LightningChartUltimate.ViewXY.XAxes[0].CoordToValue((int)pos.X, out var xValue, true);
                    _LightningChartUltimate.ViewXY.YAxes[0].CoordToValue((int)pos.Y, out var yValue, true);

                    double dx = xValue - startxValue;
                    double dy = yValue - startyValue;

                    var cp = GetCentralPoint(_PolygonSeries[IndexOf].Points);

                    PointDouble2D[] temp = new PointDouble2D[4];

                    _PolygonSeries[IndexOf].Points.CopyTo(temp, 0);
                    for (int i = 0; i < temp.Count(); i++)
                    {
                        if (MoveX)
                            temp[i].X += dx;
                        if (MoveY)
                            temp[i].Y += dy;
                    }

                    _PolygonSeries[IndexOf] = AddPolygon(temp, ChartTools.CalcGradient(colorFirst, colorLast, 0));

                    //VXY.PolygonSeries = polygonSeries;

                    startxValue = xValue;
                    startyValue = yValue;

                    EPOintValue = (int)yValue;
                    SetLines(IndexOf, yValue);
                }
            }
        }

        private void _LightningChartUltimate_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (sdown == true)
            {
                Series_MouseUp(sender, e);
            } 
        }
    }

    public class CustomBand
    {
        public double StartXMHz { get; set; }
        public double WidthMHz { get; set; }
        public double Ypos { get; set; } = -80;

        public CustomBand()
        {
        }

        public CustomBand(double StartXMHz, double WidthMHz, double Ypos = -80)
        {
            this.StartXMHz = StartXMHz;
            this.WidthMHz = WidthMHz;
            this.Ypos = Ypos;
        }

    }

}
