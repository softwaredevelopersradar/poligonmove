﻿using Arction.Wpf.SemibindableCharting;
using Arction.Wpf.SemibindableCharting.SeriesXY;
using Arction.Wpf.SemibindableCharting.Views.ViewXY;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace try_polygonMove
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private void InitializeDeploymentKey()
        {
            string deploymentKey = "lgCAABW2ij + vBNQBJABVcGRhdGVhYmxlVGlsbD0yMDE5LTA2LTE1I1JldmlzaW9uPTACgD + BCRGnn7c6dwaDiJovCk5g5nFwvJ + G60VSdCrAJ + jphM8J45NmxWE1ZpK41lW1wuI4Hz3bPIpT7aP9zZdtXrb4379WlHowJblnk8jEGJQcnWUlcFnJSl6osPYvkxfq / B0dVcthh7ezOUzf1uXfOcEJ377 / 4rwUTR0VbNTCK601EN6 / ciGJmHars325FPaj3wXDAUIehxEfwiN7aa7HcXH6RqwOF6WcD8voXTdQEsraNaTYbIqSMErzg6HFsaY5cW4IkG6TJ3iBFzXCVfvPRZDxVYMuM + Q5vztCEz5k + Luaxs + S + OQD3ELg8 + y7a / Dv0OhSQkqMDrR / o7mjauDnZVt5VRwtvDYm6kDNOsNL38Ry / tAsPPY26Ff3PDl1ItpFWZCzNS / xfDEjpmcnJOW7hmZi6X17LM66whLUTiCWjj81lpDi + VhBSMI3a2I7jmiFONUKhtD91yrOyHrCWObCdWq + F5H4gjsoP0ffEKcx658a3ZF8VhtL8d9 + B0YtxFPNBQs =";
            //Set Deployment Key for semi - bindable chart, if you use it
            Arction.Wpf.SemibindableCharting.LightningChartUltimate.SetDeploymentKey(deploymentKey);
        }

        public MainWindow()
        {
            InitializeDeploymentKey();
            InitializeComponent();

            GenerateDefaultData();
        }

        PolygonSeriesCollection polygonSeries = new PolygonSeriesCollection();
        Color colorFirst = Color.FromArgb(255, 255, 192, 0);
        Color colorLast = Color.FromArgb(255, 255, 94, 0);

        private void GenerateDefaultData()
        {
            polygonSeries.Clear();

            polygonSeries.Add(AddPolygon("-80", ChartTools.CalcGradient(colorFirst, colorLast, 0), GeneratePointDouble2D(1500), false));

            polygonSeries.Add(AddPolygon("-90", ChartTools.CalcGradient(colorFirst, colorLast, 0), GeneratePointDouble2D(1500, -90), false));

            VXY.PolygonSeries = polygonSeries;
        }

        private PointDouble2D[] GeneratePointDouble2D(double x, double y = -80)
        {
            return new PointDouble2D[] {
                new PointDouble2D(x - 500, y - 5),
                new PointDouble2D(x - 500, y +5),
                new PointDouble2D(x + 500, y +5),
                new PointDouble2D(x + 500, y-5)
            };
        }

        /// <summary>
        /// Add Polygon.
        /// </summary>
        /// <param name="title">polygon title text</param>
        /// <param name="color">polygon color</param>
        /// <param name="edgePoints">polygon edge points</param>
        /// <param name="intersectionsAllowed">Intersections allowed. Note that having lots of complex polygons with IntersectionsEnabled = True will degrade the performance a lot.</param>
        private PolygonSeries AddPolygon(string title, Color color, PointDouble2D[] edgePoints, bool intersectionsAllowed)
        {
            PolygonSeries series = new PolygonSeries();
            series.MouseHighlight = MouseOverHighlight.None;

            //series.MouseInteraction = false;

            series.Fill.GradientFill = GradientFill.Solid;
            series.Fill.Color = color;
            series.Title.Visible = true;
            series.Title.Color = Color.FromArgb(255, 30, 30, 30);
            series.Title.Text = title;
            series.Title.Shadow.Style = TextShadowStyle.DropShadow;
            series.Points = edgePoints;
            series.IntersectionsAllowed = intersectionsAllowed;

            series.MouseClick += Series_MouseClick;
            series.MouseDown += Series_MouseDown;
            series.MouseUp += Series_MouseUp;

            series.MouseDoubleClick += Series_MouseDoubleClick;

            return series;
        }

        private void Series_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
            {
                var ps = (PolygonSeries)sender;

                IndexOf = polygonSeries.IndexOf(ps);

                var temppos = e.GetPosition(chart);

                xAxis.CoordToValue((int)temppos.X, out var xValue, true);
                yAxis.CoordToValue((int)temppos.Y, out var yValue, true);


                PointDouble2D[] points4 = new PointDouble2D[polygonSeries[IndexOf].Points.Count()];
                polygonSeries[IndexOf].Points.CopyTo(points4, 0);

                PointDouble2D[] points1 = new PointDouble2D[] {
                    points4[0],
                    points4[1],
                    new PointDouble2D(xValue, points4[1].Y),
                    new PointDouble2D(xValue, points4[0].Y) };

                PointDouble2D[] points2 = new PointDouble2D[] {
                    new PointDouble2D(xValue, points4[3].Y),
                    new PointDouble2D(xValue, points4[2].Y),
                    points4[2],
                    points4[3] };

                polygonSeries.RemoveAt(IndexOf);

                polygonSeries.Insert(IndexOf, AddPolygon(indexstring(), ChartTools.CalcGradient(colorFirst, colorLast, 0), points2, false));
                polygonSeries.Insert(IndexOf, AddPolygon(indexstring(), ChartTools.CalcGradient(colorFirst, colorLast, 0), points1, false));

                VXY.PolygonSeries = polygonSeries;
            }
        }

        private void Series_MouseUp(object sender, MouseEventArgs e)
        {
            sdown = false;
            Mouse.OverrideCursor = MouseCursors.Default;
        }

        bool sdown = false;
        Point startPos;

        double startxValue;
        double startyValue;

        int IndexOf = 0;

        private void Series_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                var ps = (PolygonSeries)sender;

                Console.WriteLine(ps.Title.Text);

                IndexOf = polygonSeries.IndexOf(ps);

                Console.WriteLine(polygonSeries.IndexOf(ps));

                sdown = true;

                startPos = e.GetPosition(chart);

                xAxis.CoordToValue((int)startPos.X, out var xValue, true);
                yAxis.CoordToValue((int)startPos.Y, out var yValue, true);

                startxValue = xValue;
                startyValue = yValue;

                Mouse.OverrideCursor = MouseCursors.SizeNS;
            }

            if (e.RightButton == MouseButtonState.Pressed)
            {
                var ps = (PolygonSeries)sender;

                IndexOf = polygonSeries.IndexOf(ps);

                var temppos = e.GetPosition(chart);

                xAxis.CoordToValue((int)temppos.X, out var xValue, true);
                yAxis.CoordToValue((int)temppos.Y, out var yValue, true);


                PointDouble2D[] points4 = new PointDouble2D[polygonSeries[IndexOf].Points.Count()];
                polygonSeries[IndexOf].Points.CopyTo(points4, 0);

                PointDouble2D[] points1 = new PointDouble2D[] {
                    points4[0],
                    points4[1],
                    new PointDouble2D(xValue, points4[1].Y),
                    new PointDouble2D(xValue, points4[0].Y) };

                PointDouble2D[] points2 = new PointDouble2D[] {
                    new PointDouble2D(xValue, points4[3].Y),
                    new PointDouble2D(xValue, points4[2].Y),
                    points4[2],
                    points4[3] };

                polygonSeries.RemoveAt(IndexOf);

                polygonSeries.Insert(IndexOf, AddPolygon(indexstring(), ChartTools.CalcGradient(colorFirst, colorLast, 0), points2, false));
                polygonSeries.Insert(IndexOf, AddPolygon(indexstring(), ChartTools.CalcGradient(colorFirst, colorLast, 0), points1, false));

                VXY.PolygonSeries = polygonSeries;
            }

            if (e.MiddleButton == MouseButtonState.Pressed)
            {
                if (polygonSeries.Count > 1)
                {
                    var ps = (PolygonSeries)sender;

                    IndexOf = polygonSeries.IndexOf(ps);

                    var isunited = false;

                    if (IndexOf + 1 < polygonSeries.Count)
                    //объединить справа
                    {
                        PointDouble2D[] points1 = new PointDouble2D[4];
                        polygonSeries[IndexOf].Points.CopyTo(points1, 0);

                        PointDouble2D[] points2 = new PointDouble2D[4];
                        polygonSeries[IndexOf + 1].Points.CopyTo(points2, 0);


                        PointDouble2D[] points4 = new PointDouble2D[] {
                    points1[0],
                    points1[1],
                    new PointDouble2D(points2[2].X, points1[1].Y),
                    new PointDouble2D(points2[3].X, points1[0].Y) };

                        polygonSeries.RemoveAt(IndexOf);
                        polygonSeries.RemoveAt(IndexOf);

                        polygonSeries.Insert(IndexOf,AddPolygon(indexstring(), ChartTools.CalcGradient(colorFirst, colorLast, 0), points4, false));

                        VXY.PolygonSeries = polygonSeries;
                        isunited = true;
                    }
                    if (IndexOf == polygonSeries.Count - 1 && polygonSeries.Count > 1 && isunited == false)
                    //объединить слева
                    {
                        PointDouble2D[] points1 = new PointDouble2D[4];
                        polygonSeries[IndexOf].Points.CopyTo(points1, 0);

                        PointDouble2D[] points2 = new PointDouble2D[4];
                        polygonSeries[IndexOf -1].Points.CopyTo(points2, 0);

                        PointDouble2D[] points4 = new PointDouble2D[] {
                        new PointDouble2D(points2[0].X, points1[0].Y),
                        new PointDouble2D(points2[1].X, points1[1].Y),
                        points1[2],
                        points1[3]
                        };

                        polygonSeries.RemoveAt(IndexOf - 1);
                        polygonSeries.RemoveAt(IndexOf - 1);

                        polygonSeries.Insert(IndexOf - 1, AddPolygon(indexstring(), ChartTools.CalcGradient(colorFirst, colorLast, 0), points4, false));

                        VXY.PolygonSeries = polygonSeries;
                    }


                }
            }
        }


        private void Series_MouseClick(object sender, MouseEventArgs e)
        {
        }

        private PointDouble2D GetCentralPoint(PointDouble2D[] pointDouble2Ds)
        {
            double x = (pointDouble2Ds[2].X - pointDouble2Ds[1].X) / 2d + pointDouble2Ds[1].X;
            double y = (pointDouble2Ds[1].Y - pointDouble2Ds[0].Y) / 2d + pointDouble2Ds[0].Y;
            return new PointDouble2D(x, y);
        }

        private string indexstring()
        {
            return (IndexOf == 0) ? "-80" : "-90" ;
        }

        private void Chart_MouseMove(object sender, MouseEventArgs e)
        {
            //if (e.LeftButton == MouseButtonState.Released)
            {
                var pos = e.GetPosition(chart);

                if (sdown)
                {
                    xAxis.CoordToValue((int)pos.X, out var xValue, true);
                    yAxis.CoordToValue((int)pos.Y, out var yValue, true);

                    double dx = xValue - startxValue;
                    double dy = yValue - startyValue;

                    var cp = GetCentralPoint(polygonSeries[IndexOf].Points);

                    PointDouble2D[] temp = new PointDouble2D[4];

                    polygonSeries[IndexOf].Points.CopyTo(temp,0);
                    for (int i = 0; i < temp.Count(); i++)
                    {
                        temp[i].X += dx;
                        temp[i].Y += dy;
                    }

                    polygonSeries[IndexOf] = AddPolygon(
                        indexstring(),
                        ChartTools.CalcGradient(colorFirst, colorLast, 0),
                        temp,
                        false);

                    VXY.PolygonSeries = polygonSeries;

                    startxValue = xValue;
                    startyValue = yValue;
                }
            }
        }

        private void one_Click(object sender, RoutedEventArgs e)
        {
            var dx = 100;
            var dy = 10;

            //chart.BeginUpdate();

            for (int i = 0; i < polygonSeries[0].Points.Count(); i++)
            {
                polygonSeries[0].Points[i].X += dx;
                polygonSeries[0].Points[i].Y += dy;
            }

            VXY.PolygonSeries = polygonSeries;

            //chart.EndUpdate();

            chart.InvalidateMeasure();

            Console.Beep();
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            polygonSeries.Clear();
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            var dx = 100;
            var dy = 10;

            var cp = GetCentralPoint(polygonSeries[0].Points);

            polygonSeries.Clear();
            AddPolygon("-80", ChartTools.CalcGradient(colorFirst, colorLast, 0), GeneratePointDouble2D(cp.X + dx, cp.Y + dy), false);

            VXY.PolygonSeries = polygonSeries;
        }
    }
}
