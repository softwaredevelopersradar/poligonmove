﻿using PolygonMove;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace try_polygonMove2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        PolygonMoveClass polygonMoveClass;

        public MainWindow()
        {
            InitializeComponent();
            //polygonMoveClass = new PolygonMoveClass(chart, 25, 100, 30);

            //polygonMoveClass = new PolygonMoveClass(chart, 25, 1, 3000);
            //polygonMoveClass.isEditable = true;

            CustomBand customBand = new CustomBand(25, 3000);
            polygonMoveClass = new PolygonMoveClass(chart, new CustomBand[] { customBand });

            polygonMoveClass.isEditable = true;

            polygonMoveClass.EPOValuePro += PolygonMoveClass_EPOValuePro;
        }

        private void PolygonMoveClass_EPOValuePro(int EPO, int value, double startMHz, double endMHz)
        {
            Console.WriteLine($"EPO: {EPO}, Value: {value}, startMHz: {startMHz}, endMHz: {endMHz}");
        }

        private void one_Click(object sender, RoutedEventArgs e)
        {
            polygonMoveClass.SetEPOValue(0, -50);
        }

        private void two_Click(object sender, RoutedEventArgs e)
        {
            int[] vs = { 0, 1 };
            int[] vs1 = { -20, -30 };

            polygonMoveClass.SetEPOsValues(vs, vs1);

            List<int> vs2 = new List<int> { 2, 3 };
            List<int> vs3 = new List<int> { -50, -60 };

            polygonMoveClass.SetEPOsValues(vs2, vs3);
        }

        private void three_Click(object sender, RoutedEventArgs e)
        {
            polygonMoveClass.InitLineCollection();
        }

        private void four_Click(object sender, RoutedEventArgs e)
        {
            var GetCustomBands = polygonMoveClass.GetCustomBands();
        }

        private void Chart_MouseMove(object sender, MouseEventArgs e)
        {

        }
    }
}
